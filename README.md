# SharpSSH

#### 介绍
基于 https://github.com/jbogard/SharpSSH 开源项目
把这个项目从.Net FW 2.0改成了Standard 2.0 这意味着支持.Net Core了

SharpSSH是SSH2客户端协议套件的纯.NET实现。它提供了与SSH服务器通信的API，可以集成到任何.NET应用程序中。
