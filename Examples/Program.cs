﻿using SharpSSH.Standard;
using System;
using System.Text;
using Tamir.SharpSsh;
using Tamir.SharpSsh.jsch;

namespace Examples
{
    class Program
    {
        static void Main(string[] args)
        {
            ShellHelp shellHelp = new ShellHelp();
            try
            {
                bool isOk = false;
                string msg = "";
                (isOk, msg) = shellHelp.OpenShell("","","");
                if (!isOk)
                {
                    Console.WriteLine(msg);
                    Console.ReadLine();
                    return;
                }
                else
                {
                    Console.WriteLine("登录成功");
                    while (true)
                    {
                        Console.Write("请输入命令：");
                        string str = Console.ReadLine();
                        if (string.IsNullOrEmpty(str))
                        {
                            break;
                        }
                        Console.WriteLine(shellHelp.Shell(str));
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                shellHelp.Close();
            }

            
            Console.ReadLine();
        }
    }
    
}
